# QtWeather

QtWeather is an ambitious project to make a Qt-based weather program that can run on all platforms where Qt is supported.

# Timing and other stuff
QtWeather is designed to be a long-term project.

Development will start sometime in April/May 2019. Until then I'll be working on a smaller project to get familar with Qt and the Dark Sky API that I'll use for this project.

Between April/May 2019 I'll start planning out this project, designing some UI sketches, and getting everything ready to go.

QtWeather should be finished in late 2020.